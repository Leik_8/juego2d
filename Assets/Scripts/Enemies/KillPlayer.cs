﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class KillPlayer : MonoBehaviour
{
    public int Respawn;

    public GameObject destroyParticle;

    public SpriteRenderer spriteRenderer;

    public int lifes = 2;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    void OnTriggerEnter2D(Collider2D Other)
    {
        if (Other.CompareTag("Player"))
        {

            //SceneManager.LoadScene(Respawn);
            Other.transform.GetComponent<PlayerRespawn>().PlayerDamage();
            ScoreScript.scoreValeu = 0;

        }


        if (Other.transform.CompareTag("Bullet"))
        {
            
            destroyParticle.SetActive(true);
            spriteRenderer.enabled = false;
            Invoke("EnemyDie", 0.2f);
            CheckLife();

        }

    }

    public void CheckLife()
    {

        if (lifes == 0)
        {

            destroyParticle.SetActive(true);
            spriteRenderer.enabled = false;
            Invoke("EnemyDie", 0.2f);

        }

    }


    public void EnemyDie()
    {

        Destroy(gameObject);

    }

}
