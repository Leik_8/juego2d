﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.SceneManagement;

using Debug = UnityEngine.Debug;

public class Gate : MonoBehaviour
{

    public GameObject Player;

    // Start is called before the first frame update
    void Start()
    {

        

    }

    // Update is called once per frame
    void Update()
    {

    }

    /*public void OnCollisionEnter(Collision collision)
    {

        if(collision.gameObject.name == "Player")
        {
            Debug.Log("Collision Detected");

            SceneManager.LoadScene("Level2");

        }

    }
    */

    private void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject == Player)
        {

            SceneManager.LoadScene("Level2");

        }

    }
}

