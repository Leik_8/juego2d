﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyInTime : MonoBehaviour
{

    [SerializeField]
    float destroyTime = 0.5f;

    // Start is called before the first frame update
    void Start()
    {

        Destroy(gameObject, destroyTime);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
