﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;


public class Movement : MonoBehaviour
{

    float horizontalMove = 0;
    float verticalMove = 0;
    public float runSpeedHorizontal = 3;
    public float runSpeedVertical = 3;
    public float runSpeed = 1;
    public float JumpForce = 200f;


    Rigidbody2D rigidbody2D;

    public Joystick joystick;


    // Start is called before the first frame update
    void Start()
    {

        rigidbody2D = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {
        verticalMove = joystick.Vertical * runSpeedVertical;
        horizontalMove = joystick.Horizontal * runSpeedHorizontal;

        transform.position += new Vector3(horizontalMove, 0, 0) * Time.deltaTime * runSpeed;

        if (CrossPlatformInputManager.GetButtonDown("Jump"))
            Jump();
    }

    void Jump()
    {
        if (rigidbody2D.velocity.y == 0)
            rigidbody2D.AddForce(Vector2.up * JumpForce);

    }
}