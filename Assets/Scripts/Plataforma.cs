﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plataforma : MonoBehaviour
{

    public SpriteRenderer sprite;

    public float speed = 0.5f;

    private float waitTime;

    public Transform[] moveSpot;

    public float startWaitTime = 1;

    private int i = 0;

    private Vector2 actualPos;


    // Start is called before the first frame update
    void Start()
    {

        waitTime = startWaitTime;

    }

    // Update is called once per frame
    void Update()
    {


       

        transform.position = Vector2.MoveTowards(transform.position, moveSpot[i].transform.position, speed * Time.deltaTime);

        if (Vector2.Distance(transform.position, moveSpot[i].transform.position) < 0.1f)
        {

            if (waitTime <= 0)
            {

                if (moveSpot[i] != moveSpot[moveSpot.Length - 1])
                {

                    i++;

                }
                else
                {

                    i = 0;

                }

                waitTime = startWaitTime;

            }
            else
            {

                waitTime -= Time.deltaTime;

            }

        }

    }

    
}
