﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;


public class MENU : MonoBehaviour
{
    public AudioSource Clip;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SampleScene()
    {
        SceneManager.LoadScene("SampleScene");
    }

    public void ExitGame()
    {

        Application.Quit();

    }

    public void PlaySoundButton()
    {

        Clip.Play();

    }


}
