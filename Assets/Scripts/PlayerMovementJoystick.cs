﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.Audio;


public class PlayerMovementJoystick : MonoBehaviour
{

    private float horizontalMove = 0f;

    public Joystick joystick;

    public float runSpeedHorizontal = 2;

    public float runSpeed = 1.25f;

    public float jumpSpeed = 3f;

    public Transform groundCheckPoint;

    public float groundCheckRadius;

    public bool isTouchingGround;

    public LayerMask groundLayers;

    Rigidbody2D rb2D;

    public SpriteRenderer spriteRenderer;

    public Animator animator;

    private float FireSpeed = 500f;

    public Transform barrel;

    public Rigidbody2D bullet;

    public AudioSource Shootclip;

    public AudioSource RunClip;





    // Start is called before the first frame update
    void Start()
    {

        rb2D = GetComponent<Rigidbody2D>();
        
    }

    // Update is called once per frame
    void Update()
    {

        isTouchingGround = Physics2D.OverlapCircle(groundCheckPoint.position, groundCheckRadius, groundLayers);

        if (horizontalMove > 0)
        {
            transform.eulerAngles = new Vector3(0, 0, 0);
            animator.SetBool("Run", true);
            

        }
        else if (horizontalMove < 0)
        {
            transform.eulerAngles = new Vector3(0, -180, 0);
            animator.SetBool("Run", true);
            

        }
        else if (horizontalMove == 0)
        {
            animator.SetBool("Run", false);
            RunClip.Play();

        }


        if (isTouchingGround == true)
        {
            animator.SetBool("Jump", false);
        }

        else if (isTouchingGround == false)
        {
            animator.SetBool("Jump", true);
        }

        if (CrossPlatformInputManager.GetButtonDown("Fire1"))
            Fire();

        if (horizontalMove > 0 && isTouchingGround == false)
        {
            animator.SetBool("Jump", true);
        }
        else if(horizontalMove < 0 && isTouchingGround == false)
        {
            animator.SetBool("Jump", true);
        }
        else if(horizontalMove == 0 && isTouchingGround == true)
        {
            animator.SetBool("Jump", false);
        }

    }

    void FixedUpdate()
    {
        
        horizontalMove = joystick.Horizontal * runSpeedHorizontal;
        transform.position += new Vector3(horizontalMove, 0, 0) * Time.deltaTime * runSpeed;
        

    }


    public void Jump()
    {
        if (isTouchingGround == true)
        {

            rb2D.velocity = new Vector2(rb2D.velocity.x, jumpSpeed);

        }
        

        //rb2D.velocity = new Vector2(rb2D.velocity.x, jumpSpeed);

    }

    public void Fire()
    {

        var fireBullet = Instantiate (bullet, barrel.position, barrel.rotation);
        fireBullet.AddForce(barrel.up * FireSpeed);

        Shootclip.Play();
    }

    void OnCollisionEnter2D(Collision2D col)
    {

        if (col.gameObject.name.Equals("Platform"))
        {

            this.transform.parent = col.transform;

        }

    }

    void OnCollisionExit2D(Collision2D col)
    {

        if (col.gameObject.name.Equals("Platform"))
        {

            this.transform.parent = null;

        }

    }


}
